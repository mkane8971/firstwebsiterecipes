from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Avg

# Get the name of the class used for the User
USER_MODEL = settings.AUTH_USER_MODEL
# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(blank=True)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title #this puts in admin to show recipe title rather than object number
    author = models.ForeignKey(
        USER_MODEL,
        related_name="recipes",
        on_delete=models.CASCADE,
        null=True,
    )
    # class Meta:
    #     ordering = ["author"]
    # steps

class RecipeStep(models.Model):
    step_number = models.PositiveIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete= models.CASCADE,
    )
        #new_recipe = Recipe()
    #example_recipe = Recipe.objects.get(id=id)
    #example_recipe_steps = example_recipe.steps
     #string after = needs to match variable in class
    class Meta:
        ordering = [ "step_number"]

    #recipe.steps.all() --> []
    def recipe_title(self):
        return self.recipe.title

class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        Recipe,
        related_name = "ingredients",
        on_delete= models.CASCADE,
    )

class Review(models.Model):
    author = models.ForeignKey(
        USER_MODEL,
        related_name="reviews",
        on_delete=models.CASCADE,
        null=True,
    )
    recipe = models.ForeignKey(
        Recipe,
        related_name = "reviews",
        on_delete= models.CASCADE,
    )
    review = models.CharField(max_length=100)
