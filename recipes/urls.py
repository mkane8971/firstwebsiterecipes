from django.urls import path
from recipes.views import create_recipe, recipe_list, show_recipe, edit_post, my_recipe_list
from accounts.views import signup
from django.shortcuts import redirect

def redirect_to_recipe_list(home_page):
    return redirect("recipe_list")

urlpatterns = [
    path("recipes/", recipe_list, name="recipe_list"),
    path("recipes/create/", create_recipe, name="create_recipe"),
    path("recipes/<int:id>/", show_recipe, name="show_recipe"),
    path("recipes/<int:id>/edit", edit_post, name="edit_post"),
    path("", redirect_to_recipe_list, name="home_page"),
    path("mine/", my_recipe_list, name="my_recipe_list"),

]
