from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required
# Create your views here.
# def create_thing(request):
#     if request.method == "POST":
#         # POST is when the person has submitted the form
#         # We should use the form to validate the values
#         #   and save them to the database
#         # If all goes well, we can redirect the browser
#         #   to another page and leave the function
#     else:
#         # Create an instance of the Django model form class

#     # Put the form in the context
#     # Render the HTML template with the form
@login_required
def create_recipe(request):
    if request.method == "POST":

            form = RecipeForm(request.POST)
            if form.is_valid():
                recipe = form.save(False)
                recipe.author = request.user
                recipe.save()
            return redirect("recipe_list")

    else:
        form = RecipeForm()
    context = {
        "form": form,
    }
    return render(request, "recipes/create.html", context)

def list_recipes(request):
    return render(request, "recipes/list.html")

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)


def recipe_list(request):
    recipes= Recipe.objects.all()
    context = {
        "recipe_list":recipes,
    }
    return render(request, "recipes/list.html", context)

def edit_post(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)  #this makes the form update the object recipe with instance=recipe
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
      "form": form,
    }
    return render(request, "recipes/edit.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
